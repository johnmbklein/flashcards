$(document).ready(function() {

  $(".subject").click(function() {
    $(this).toggleClass("hidden");
    $(this).next().toggleClass("hidden");
  });

  $(".definition").click(function() {
    $(this).toggleClass("hidden");
    $(this).prev().toggleClass("hidden");
  });

});
